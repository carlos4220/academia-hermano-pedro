﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreacionAsignación
{
    class Profesor
    {
        private int id_pro;
        private string pnombre;
        private string snombre;
        private string papellido;
        private string sapellido;
        private string dpi;
        private string telefono;
        private string direccion;
      

        public int Id_pro { get; set; }
        public string Pnombre { get; set; }
        public string Snombre { get; set; }
        public string Papellido { get; set; }
        public string Sapellido { get; set; }
        public string Dpi { get; set; }
        public string Telefono { get; set; }
        public string Direccion { get; set; }
    }
}

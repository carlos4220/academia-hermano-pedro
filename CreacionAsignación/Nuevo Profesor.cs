﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CreacionAsignación
{
    public partial class Nuevo_Profesor : Form
    {
        Profesor profesor = new Profesor();
        public Nuevo_Profesor()
        {
            InitializeComponent();
        }

        private void btn_return_Click(object sender, EventArgs e)
        {
            Form formualrio = new Profesores();
            formualrio.Show();
            this.Hide();
        }

        private void btn_Home_Click(object sender, EventArgs e)
        {
            Form formualrio = new Form1();
            formualrio.Show();
            this.Hide();
        }

        private void btn_exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btn_save_Click(object sender, EventArgs e)
        {
            profesor.Id_pro = Convert.ToInt32(txt_id.Text);
            profesor.Pnombre = txt_name.Text;
            profesor.Snombre = txt_sname.Text;
            profesor.Papellido = txt_papellido.Text;
            profesor.Sapellido = txt_sapellido.Text;
            profesor.Dpi = txt_DPI.Text;
            profesor.Direccion = txt_dir.Text;
            profesor.Telefono = txt_tel.Text;
        }

        private void btn_nuevo_Click(object sender, EventArgs e)
        {
            txt_id.Clear();
            txt_name.Clear();
            txt_sname.Clear();
            txt_papellido.Clear();
            txt_sapellido.Clear();
            txt_DPI.Clear();
            txt_dir.Clear();
            txt_tel.Clear();
        }
    }
}

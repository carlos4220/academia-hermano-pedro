﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CreacionAsignación
{
    public partial class NuevoAlumno : Form
    {
        Usuario usuario = new Usuario();


        public NuevoAlumno()
        {
            InitializeComponent();
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void btn_Home_Click(object sender, EventArgs e)
        {
            Form formualrio = new Form1();
            formualrio.Show();
            this.Hide();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btn_save_Click(object sender, EventArgs e)
        {
            usuario.Id = Convert.ToInt32(txt_id.Text);
            usuario.PrimerNombre = txtPnombre.Text;
            usuario.SegundoNombre = txt_Snombre.Text;
            usuario.PrimerApellido = txt_Papellido.Text;
            usuario.SegundoApellido = txt_Sapellido.Text;
            usuario.Direccion = txt_dir.Text;
            usuario.Telefono = txt_tel.Text;
            MessageBox.Show("Su guardo el alumno correctamente");
        }

        private void btn_nuevo_Click(object sender, EventArgs e)
        {
            txt_id.Clear();
            txtPnombre.Clear();
            txt_Snombre.Clear();
            txt_Papellido.Clear();
            txt_Sapellido.Clear();
            txt_dir.Clear();
            txt_tel.Clear();

        }

        private void btn_return_Click(object sender, EventArgs e)
        {
            Form formualrio = new Alumnos();
            formualrio.Show();
            this.Hide();
        }

        private void NuevoAlumno_Load(object sender, EventArgs e)
        {

        }
    }
}

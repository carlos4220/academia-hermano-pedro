﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CreacionAsignación
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btn_Profesor_Click(object sender, EventArgs e)
        {
            Form formualrio = new Profesores();
            formualrio.Show();
            this.Hide();
        }

        private void btn_exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btn_alumno_Click(object sender, EventArgs e)
        {
            Form formualrio = new Alumnos();
            formualrio.Show();
            this.Hide();
        }

        private void btn_exit_Click_1(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btn_curso_Click(object sender, EventArgs e)
        {
            Form formualrio = new Cursos();
            formualrio.Show();
            this.Hide();
        }
    }
}

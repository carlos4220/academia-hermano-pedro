﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CreacionAsignación
{
    public partial class Profesores : Form
    {
        public Profesores()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form formualrio = new Form1();
            formualrio.Show();
            this.Hide();
        }

        private void btn_addP_Click(object sender, EventArgs e)
        {
            Form formualrio = new Nuevo_Profesor();
            formualrio.Show();
            this.Hide();
        }
    }
}

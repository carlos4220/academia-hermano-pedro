﻿namespace CreacionAsignación
{
    partial class Alumnos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_addA = new System.Windows.Forms.Button();
            this.btn_asigA = new System.Windows.Forms.Button();
            this.btn_return = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_addA
            // 
            this.btn_addA.Location = new System.Drawing.Point(383, 40);
            this.btn_addA.Name = "btn_addA";
            this.btn_addA.Size = new System.Drawing.Size(162, 72);
            this.btn_addA.TabIndex = 2;
            this.btn_addA.Text = "Nuevo Alumno ";
            this.btn_addA.UseVisualStyleBackColor = true;
            this.btn_addA.Click += new System.EventHandler(this.btn_addA_Click);
            // 
            // btn_asigA
            // 
            this.btn_asigA.Location = new System.Drawing.Point(383, 149);
            this.btn_asigA.Name = "btn_asigA";
            this.btn_asigA.Size = new System.Drawing.Size(162, 72);
            this.btn_asigA.TabIndex = 3;
            this.btn_asigA.Text = "Asignar Curso ";
            this.btn_asigA.UseVisualStyleBackColor = true;
            // 
            // btn_return
            // 
            this.btn_return.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btn_return.Image = global::CreacionAsignación.Properties.Resources.descarga;
            this.btn_return.Location = new System.Drawing.Point(402, 259);
            this.btn_return.Name = "btn_return";
            this.btn_return.Size = new System.Drawing.Size(129, 114);
            this.btn_return.TabIndex = 4;
            this.btn_return.UseVisualStyleBackColor = false;
            this.btn_return.Click += new System.EventHandler(this.btn_return_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::CreacionAsignación.Properties.Resources.images;
            this.pictureBox1.Location = new System.Drawing.Point(77, 77);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(231, 221);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // Alumnos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btn_return);
            this.Controls.Add(this.btn_asigA);
            this.Controls.Add(this.btn_addA);
            this.Controls.Add(this.pictureBox1);
            this.Name = "Alumnos";
            this.Text = "Academia Hermano Pedro";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btn_addA;
        private System.Windows.Forms.Button btn_asigA;
        private System.Windows.Forms.Button btn_return;
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CreacionAsignación
{
    public partial class Asignacion_Alum : Form
    {
        public Asignacion_Alum()
        {
            InitializeComponent();
        }
        // Haceme la conexion de la base de datos aca para filtrar los alumnos y cursos
        public int selec;
        private void Asignacion_Alum_Load(object sender, EventArgs e)
        {
            dataGridView2.Columns[0].Visible = false;
            //Hace la conexion de la base de datos para jalar los alumnos creados

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            selec = Convert.ToInt32(dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[0].Value.ToString());
            string nombre = dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[1].Value.ToString();
            string apellido = dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[2].Value.ToString();


            dataGridView2.Rows.Add(selec, nombre, apellido);
            dataGridView1.DataSource = null;
            dataGridView1.Rows.Clear();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // Haceme la conexion con la base de datos para que al momento de buscar
            // me muestre los alumnos creados
        }

        private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void bn_add_Click(object sender, EventArgs e)
        {
            if (dataGridView2.Rows.Count < 1)
            {
                MessageBox.Show("Debe agregar por lo menos un alumno");
            }
            else
            {
                try
                {
                    for (int i = 0; i < dataGridView2.Rows.Count; i++)
                    {
                        //Haces la conexion con la base de datos para cargar los estudiantes al curso
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                dataGridView2.DataSource = null;
                dataGridView2.Rows.Clear();
            }
        }

        private void btn_exit_Click(object sender, EventArgs e)
        {
            Form formualrio = new Form1();
            formualrio.Show();
            this.Hide();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CreacionAsignación
{
    public partial class Alumnos : Form
    {
        public Alumnos()
        {
            InitializeComponent();
        }

        private void btn_return_Click(object sender, EventArgs e)
        {
            Form formualrio = new Form1();
            formualrio.Show();
            this.Hide();
        }

        private void btn_addA_Click(object sender, EventArgs e)
        {
            Form formualrio = new NuevoAlumno();
            formualrio.Show();
            this.Hide();
        }
    }
}

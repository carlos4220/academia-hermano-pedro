﻿namespace CreacionAsignación
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_curso = new System.Windows.Forms.Button();
            this.btn_exit = new System.Windows.Forms.Button();
            this.btn_alumno = new System.Windows.Forms.Button();
            this.btn_Profesor = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn_curso
            // 
            this.btn_curso.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btn_curso.Image = global::CreacionAsignación.Properties.Resources.icono_cursos_circular1_250x250___copia;
            this.btn_curso.Location = new System.Drawing.Point(554, 94);
            this.btn_curso.Name = "btn_curso";
            this.btn_curso.Size = new System.Drawing.Size(246, 219);
            this.btn_curso.TabIndex = 22;
            this.btn_curso.UseVisualStyleBackColor = false;
            this.btn_curso.Click += new System.EventHandler(this.btn_curso_Click);
            // 
            // btn_exit
            // 
            this.btn_exit.Image = global::CreacionAsignación.Properties.Resources._50599960_piso_icono_salir_verde_y_círculo_verde;
            this.btn_exit.Location = new System.Drawing.Point(354, 335);
            this.btn_exit.Name = "btn_exit";
            this.btn_exit.Size = new System.Drawing.Size(110, 103);
            this.btn_exit.TabIndex = 21;
            this.btn_exit.UseVisualStyleBackColor = true;
            this.btn_exit.Click += new System.EventHandler(this.btn_exit_Click_1);
            // 
            // btn_alumno
            // 
            this.btn_alumno.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btn_alumno.Image = global::CreacionAsignación.Properties.Resources.images;
            this.btn_alumno.Location = new System.Drawing.Point(12, 94);
            this.btn_alumno.Name = "btn_alumno";
            this.btn_alumno.Size = new System.Drawing.Size(246, 219);
            this.btn_alumno.TabIndex = 1;
            this.btn_alumno.UseVisualStyleBackColor = false;
            this.btn_alumno.Click += new System.EventHandler(this.btn_alumno_Click);
            // 
            // btn_Profesor
            // 
            this.btn_Profesor.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btn_Profesor.Image = global::CreacionAsignación.Properties.Resources.images__1_;
            this.btn_Profesor.Location = new System.Drawing.Point(280, 94);
            this.btn_Profesor.Name = "btn_Profesor";
            this.btn_Profesor.Size = new System.Drawing.Size(246, 219);
            this.btn_Profesor.TabIndex = 0;
            this.btn_Profesor.UseVisualStyleBackColor = false;
            this.btn_Profesor.Click += new System.EventHandler(this.btn_Profesor_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btn_curso);
            this.Controls.Add(this.btn_exit);
            this.Controls.Add(this.btn_alumno);
            this.Controls.Add(this.btn_Profesor);
            this.Name = "Form1";
            this.Text = "Academia Hermano Pedro";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_Profesor;
        private System.Windows.Forms.Button btn_alumno;
        private System.Windows.Forms.Button btn_exit;
        private System.Windows.Forms.Button btn_curso;
    }
}


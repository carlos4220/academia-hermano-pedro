﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreacionAsignación
{
    class Curso
    {
        private int id_curso;
        private int id_ciclo;
        private string nombre;
        private int hora;
        private int costo;

        public int Id_Curso { get; set; }
        public int Id_Ciclo { get; set; }
        public string Nombre { get; set; }
        public int Hora { get; set; }
        public int Costo { get; set; }


    }
}

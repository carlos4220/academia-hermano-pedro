﻿namespace CreacionAsignación
{
    partial class NuevoAlumno
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txt_id = new System.Windows.Forms.TextBox();
            this.txtPnombre = new System.Windows.Forms.TextBox();
            this.txt_Snombre = new System.Windows.Forms.TextBox();
            this.txt_Papellido = new System.Windows.Forms.TextBox();
            this.txt_tel = new System.Windows.Forms.TextBox();
            this.txt_Sapellido = new System.Windows.Forms.TextBox();
            this.txt_dir = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.btn_save = new System.Windows.Forms.Button();
            this.btn_nuevo = new System.Windows.Forms.Button();
            this.btn_return = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.btn_Home = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // txt_id
            // 
            this.txt_id.Location = new System.Drawing.Point(315, 33);
            this.txt_id.Name = "txt_id";
            this.txt_id.Size = new System.Drawing.Size(208, 20);
            this.txt_id.TabIndex = 3;
            // 
            // txtPnombre
            // 
            this.txtPnombre.Location = new System.Drawing.Point(315, 76);
            this.txtPnombre.Name = "txtPnombre";
            this.txtPnombre.Size = new System.Drawing.Size(208, 20);
            this.txtPnombre.TabIndex = 4;
            // 
            // txt_Snombre
            // 
            this.txt_Snombre.Location = new System.Drawing.Point(315, 117);
            this.txt_Snombre.Name = "txt_Snombre";
            this.txt_Snombre.Size = new System.Drawing.Size(208, 20);
            this.txt_Snombre.TabIndex = 5;
            // 
            // txt_Papellido
            // 
            this.txt_Papellido.Location = new System.Drawing.Point(315, 154);
            this.txt_Papellido.Name = "txt_Papellido";
            this.txt_Papellido.Size = new System.Drawing.Size(208, 20);
            this.txt_Papellido.TabIndex = 6;
            this.txt_Papellido.TextChanged += new System.EventHandler(this.textBox4_TextChanged);
            // 
            // txt_tel
            // 
            this.txt_tel.Location = new System.Drawing.Point(315, 265);
            this.txt_tel.Name = "txt_tel";
            this.txt_tel.Size = new System.Drawing.Size(208, 20);
            this.txt_tel.TabIndex = 7;
            // 
            // txt_Sapellido
            // 
            this.txt_Sapellido.Location = new System.Drawing.Point(315, 193);
            this.txt_Sapellido.Name = "txt_Sapellido";
            this.txt_Sapellido.Size = new System.Drawing.Size(208, 20);
            this.txt_Sapellido.TabIndex = 8;
            // 
            // txt_dir
            // 
            this.txt_dir.Location = new System.Drawing.Point(315, 228);
            this.txt_dir.Name = "txt_dir";
            this.txt_dir.Size = new System.Drawing.Size(208, 20);
            this.txt_dir.TabIndex = 9;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(291, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(18, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "ID";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(233, 83);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Primer Nombre";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(234, 161);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "Primer Apellido";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(219, 124);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(90, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "Segundo Nombre";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(219, 200);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(90, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "Segundo Apellido";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(257, 235);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 13);
            this.label6.TabIndex = 15;
            this.label6.Text = "Direccion";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(260, 272);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(49, 13);
            this.label7.TabIndex = 16;
            this.label7.Text = "Telefono";
            // 
            // btn_save
            // 
            this.btn_save.Location = new System.Drawing.Point(31, 200);
            this.btn_save.Name = "btn_save";
            this.btn_save.Size = new System.Drawing.Size(132, 48);
            this.btn_save.TabIndex = 17;
            this.btn_save.Text = "Guardar Alumno";
            this.btn_save.UseVisualStyleBackColor = true;
            this.btn_save.Click += new System.EventHandler(this.btn_save_Click);
            // 
            // btn_nuevo
            // 
            this.btn_nuevo.Location = new System.Drawing.Point(31, 254);
            this.btn_nuevo.Name = "btn_nuevo";
            this.btn_nuevo.Size = new System.Drawing.Size(132, 49);
            this.btn_nuevo.TabIndex = 18;
            this.btn_nuevo.Text = "Crear Nuevo";
            this.btn_nuevo.UseVisualStyleBackColor = true;
            this.btn_nuevo.Click += new System.EventHandler(this.btn_nuevo_Click);
            // 
            // btn_return
            // 
            this.btn_return.Image = global::CreacionAsignación.Properties.Resources.descarga__1_;
            this.btn_return.Location = new System.Drawing.Point(236, 325);
            this.btn_return.Name = "btn_return";
            this.btn_return.Size = new System.Drawing.Size(110, 103);
            this.btn_return.TabIndex = 21;
            this.btn_return.UseVisualStyleBackColor = true;
            this.btn_return.Click += new System.EventHandler(this.btn_return_Click);
            // 
            // button3
            // 
            this.button3.Image = global::CreacionAsignación.Properties.Resources._50599960_piso_icono_salir_verde_y_círculo_verde;
            this.button3.Location = new System.Drawing.Point(498, 325);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(110, 103);
            this.button3.TabIndex = 20;
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // btn_Home
            // 
            this.btn_Home.Image = global::CreacionAsignación.Properties.Resources.descarga;
            this.btn_Home.Location = new System.Drawing.Point(366, 325);
            this.btn_Home.Name = "btn_Home";
            this.btn_Home.Size = new System.Drawing.Size(110, 103);
            this.btn_Home.TabIndex = 19;
            this.btn_Home.UseVisualStyleBackColor = true;
            this.btn_Home.Click += new System.EventHandler(this.btn_Home_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::CreacionAsignación.Properties.Resources.unnamed1;
            this.pictureBox1.Location = new System.Drawing.Point(22, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(141, 140);
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // NuevoAlumno
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btn_return);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.btn_Home);
            this.Controls.Add(this.btn_nuevo);
            this.Controls.Add(this.btn_save);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txt_dir);
            this.Controls.Add(this.txt_Sapellido);
            this.Controls.Add(this.txt_tel);
            this.Controls.Add(this.txt_Papellido);
            this.Controls.Add(this.txt_Snombre);
            this.Controls.Add(this.txtPnombre);
            this.Controls.Add(this.txt_id);
            this.Controls.Add(this.pictureBox1);
            this.Name = "NuevoAlumno";
            this.Text = "Academia Hermano Pedro";
            this.Load += new System.EventHandler(this.NuevoAlumno_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox txt_id;
        private System.Windows.Forms.TextBox txtPnombre;
        private System.Windows.Forms.TextBox txt_Snombre;
        private System.Windows.Forms.TextBox txt_Papellido;
        private System.Windows.Forms.TextBox txt_tel;
        private System.Windows.Forms.TextBox txt_Sapellido;
        private System.Windows.Forms.TextBox txt_dir;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btn_save;
        private System.Windows.Forms.Button btn_nuevo;
        private System.Windows.Forms.Button btn_Home;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button btn_return;
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CreacionAsignación
{
    public partial class Cursos : Form
    {
        Curso curso = new Curso();
        public Cursos()
        {
            InitializeComponent();
        }

        private void btn_Home_Click(object sender, EventArgs e)
        {
            Form formualrio = new Form1();
            formualrio.Show();
            this.Hide();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btn_Save_Click(object sender, EventArgs e)
        {
            curso.Id_Curso = Convert.ToInt32(txt_IdCurso.Text);
            curso.Id_Ciclo = Convert.ToInt32(txt_IdCurso.Text);
            curso.Nombre = txt_name.Text;
            curso.Hora = Convert.ToInt32(txt_Time.Text);
            curso.Costo = Convert.ToInt32(txt_cost.Text);

        }

        private void btn_New_Click(object sender, EventArgs e)
        {
            txt_IdCurso.Clear();
            txt_IdCiclo.Clear();
            txt_name.Clear();
            txt_Time.Clear();
            txt_cost.Clear();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Conectame con la base de datos los profesores para que se puedan seleccionar al crear el curso

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form formualrio = new Asignacion_Alum();
            formualrio.Show();
            this.Hide();
        }

        private void Cursos_Load(object sender, EventArgs e)
        {

        }
    }
}

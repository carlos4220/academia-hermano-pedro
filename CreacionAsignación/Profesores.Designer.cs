﻿namespace CreacionAsignación
{
    partial class Profesores
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_addP = new System.Windows.Forms.Button();
            this.btn_return = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_addP
            // 
            this.btn_addP.Location = new System.Drawing.Point(481, 157);
            this.btn_addP.Name = "btn_addP";
            this.btn_addP.Size = new System.Drawing.Size(162, 72);
            this.btn_addP.TabIndex = 1;
            this.btn_addP.Text = "Nuevo Profesor ";
            this.btn_addP.UseVisualStyleBackColor = true;
            this.btn_addP.Click += new System.EventHandler(this.btn_addP_Click);
            // 
            // btn_return
            // 
            this.btn_return.BackColor = System.Drawing.SystemColors.ControlLight;
            this.btn_return.Image = global::CreacionAsignación.Properties.Resources.descarga;
            this.btn_return.Location = new System.Drawing.Point(499, 276);
            this.btn_return.Name = "btn_return";
            this.btn_return.Size = new System.Drawing.Size(124, 104);
            this.btn_return.TabIndex = 3;
            this.btn_return.UseVisualStyleBackColor = false;
            this.btn_return.Click += new System.EventHandler(this.button1_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::CreacionAsignación.Properties.Resources.images__1_;
            this.pictureBox1.Location = new System.Drawing.Point(131, 84);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(224, 228);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // Profesores
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btn_return);
            this.Controls.Add(this.btn_addP);
            this.Controls.Add(this.pictureBox1);
            this.Name = "Profesores";
            this.Text = "Academia Hermano Pedro";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btn_addP;
        private System.Windows.Forms.Button btn_return;
    }
}
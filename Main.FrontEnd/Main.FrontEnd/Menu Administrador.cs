﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CreacionAsignación;

namespace Main.FrontEnd
{
    public partial class MenudelAdministrador : Form
    {
        public MenudelAdministrador()
        {
            InitializeComponent();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            //ACA SE LLAMA EL FORMULARIO DEL REGISTRO DE CURSOS
            Cursos ventana = new Cursos();
            ventana.ShowDialog();
        }

        private void BtnRegistroDeCatedrativos_Click(object sender, EventArgs e)
        {
            Nuevo_Profesor ventana = new Nuevo_Profesor();
            ventana.ShowDialog();
        }

        private void BtnRegistroDeAlumnos_Click(object sender, EventArgs e)
        {
            //ACA SE LLAMA EL FORMULARIO DEL REGISTRO DE ALUMNOS
            NuevoAlumno ventana = new NuevoAlumno();
            ventana.ShowDialog();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.Location = Screen.PrimaryScreen.WorkingArea.Location;
            this.Size = Screen.PrimaryScreen.WorkingArea.Size;
        }

        private void sizedistribution(object sender, EventArgs e)
        {
            namearea.Width = this.Width;
            botonarea.Width = this.Width;
            imgback.Width = this.Width;
            panelcontenido.Width = this.Width;
        }

        private void rbotons(object sender, EventArgs e)
        {
            if (this.Width <= 1000)
            {
                botons.Location = Screen.PrimaryScreen.WorkingArea.Location;
            }
            else
            {
                botons.Left = this.Left + 300;
            }
        }

        private void rcentrarc(object sender, EventArgs e)
        {
            if (this.Width <= 1000)
            {
                contenido.Location = Screen.PrimaryScreen.WorkingArea.Location;
            }
            else
            {
                contenido.Left = this.Left + 300;
            }
        }

        private void imgback_Click(object sender, EventArgs e)
        {

        }

        private void btnRegistroDeCatedrativos_MouseHover(object sender, EventArgs e)
        {
            InfoCatedratico.Text = "Lleve un registro del personal docente. Con datos, registros, informes y detalle de las asignaciones";
        }

        private void button1_MouseHover(object sender, EventArgs e)
        {
            InfoCurso.Text = "Lleve un registro con el detalle de los cursos, estadisticas y horarios ";
        }

        private void btnRegistroDeAlumnos_MouseHover(object sender, EventArgs e)
        {
            InfoAlumno.Text = "Registro de los alumnos inscritos, reportes, visualizacion de notas, estadisticas generales.";
        }

        private void panel2_MouseHover(object sender, EventArgs e)
        {
            InfoCatedratico.Text = "Presione en Registrar Catedratico";
        }

        private void panel3_MouseMove(object sender, MouseEventArgs e)
        {
            InfoCurso.Text = "Presione en Registrar Curso";
        }

        private void panel4_MouseHover(object sender, EventArgs e)
        {
            InfoAlumno.Text = "Presione en Registrar Alumno";
        }
    } 
}

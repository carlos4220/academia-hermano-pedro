﻿namespace Main.FrontEnd
{
    partial class MenudelAdministrador
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MenudelAdministrador));
            this.button1 = new System.Windows.Forms.Button();
            this.btnRegistroDeAlumnos = new System.Windows.Forms.Button();
            this.btnRegistroDeCatedrativos = new System.Windows.Forms.Button();
            this.botonarea = new System.Windows.Forms.Panel();
            this.botons = new System.Windows.Forms.Panel();
            this.namearea = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.text1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panelcontenido = new System.Windows.Forms.Panel();
            this.contenido = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.InfoAlumno = new System.Windows.Forms.RichTextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.InfoCurso = new System.Windows.Forms.RichTextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.InfoCatedratico = new System.Windows.Forms.RichTextBox();
            this.imgback = new System.Windows.Forms.PictureBox();
            this.botonarea.SuspendLayout();
            this.botons.SuspendLayout();
            this.namearea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panelcontenido.SuspendLayout();
            this.contenido.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgback)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(50)))), ((int)(((byte)(140)))));
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Verdana", 13.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(250, 0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(250, 50);
            this.button1.TabIndex = 0;
            this.button1.Text = "Registrar Cursos";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            this.button1.MouseHover += new System.EventHandler(this.button1_MouseHover);
            // 
            // btnRegistroDeAlumnos
            // 
            this.btnRegistroDeAlumnos.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(50)))), ((int)(((byte)(46)))));
            this.btnRegistroDeAlumnos.FlatAppearance.BorderSize = 0;
            this.btnRegistroDeAlumnos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRegistroDeAlumnos.Font = new System.Drawing.Font("Verdana", 13.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRegistroDeAlumnos.ForeColor = System.Drawing.Color.White;
            this.btnRegistroDeAlumnos.Location = new System.Drawing.Point(500, 0);
            this.btnRegistroDeAlumnos.Name = "btnRegistroDeAlumnos";
            this.btnRegistroDeAlumnos.Size = new System.Drawing.Size(250, 50);
            this.btnRegistroDeAlumnos.TabIndex = 1;
            this.btnRegistroDeAlumnos.Text = "Registrar Alumnos";
            this.btnRegistroDeAlumnos.UseVisualStyleBackColor = false;
            this.btnRegistroDeAlumnos.Click += new System.EventHandler(this.BtnRegistroDeAlumnos_Click);
            this.btnRegistroDeAlumnos.MouseHover += new System.EventHandler(this.btnRegistroDeAlumnos_MouseHover);
            // 
            // btnRegistroDeCatedrativos
            // 
            this.btnRegistroDeCatedrativos.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(145)))), ((int)(((byte)(144)))));
            this.btnRegistroDeCatedrativos.FlatAppearance.BorderSize = 0;
            this.btnRegistroDeCatedrativos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRegistroDeCatedrativos.Font = new System.Drawing.Font("Verdana", 13.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRegistroDeCatedrativos.ForeColor = System.Drawing.Color.White;
            this.btnRegistroDeCatedrativos.Location = new System.Drawing.Point(0, 0);
            this.btnRegistroDeCatedrativos.Name = "btnRegistroDeCatedrativos";
            this.btnRegistroDeCatedrativos.Size = new System.Drawing.Size(250, 50);
            this.btnRegistroDeCatedrativos.TabIndex = 2;
            this.btnRegistroDeCatedrativos.Text = "Registrar Catedraticos";
            this.btnRegistroDeCatedrativos.UseVisualStyleBackColor = false;
            this.btnRegistroDeCatedrativos.Click += new System.EventHandler(this.BtnRegistroDeCatedrativos_Click);
            this.btnRegistroDeCatedrativos.MouseHover += new System.EventHandler(this.btnRegistroDeCatedrativos_MouseHover);
            // 
            // botonarea
            // 
            this.botonarea.AutoSize = true;
            this.botonarea.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(89)))), ((int)(((byte)(87)))));
            this.botonarea.Controls.Add(this.botons);
            this.botonarea.Location = new System.Drawing.Point(0, 400);
            this.botonarea.Name = "botonarea";
            this.botonarea.Size = new System.Drawing.Size(1350, 53);
            this.botonarea.TabIndex = 3;
            this.botonarea.Resize += new System.EventHandler(this.rbotons);
            // 
            // botons
            // 
            this.botons.Controls.Add(this.button1);
            this.botons.Controls.Add(this.btnRegistroDeCatedrativos);
            this.botons.Controls.Add(this.btnRegistroDeAlumnos);
            this.botons.Location = new System.Drawing.Point(300, 0);
            this.botons.Name = "botons";
            this.botons.Size = new System.Drawing.Size(750, 50);
            this.botons.TabIndex = 3;
            // 
            // namearea
            // 
            this.namearea.AutoSize = true;
            this.namearea.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(110)))), ((int)(((byte)(189)))));
            this.namearea.Controls.Add(this.label2);
            this.namearea.Controls.Add(this.label1);
            this.namearea.Controls.Add(this.text1);
            this.namearea.Controls.Add(this.pictureBox1);
            this.namearea.Location = new System.Drawing.Point(0, 0);
            this.namearea.Name = "namearea";
            this.namearea.Size = new System.Drawing.Size(1350, 103);
            this.namearea.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.label2.Font = new System.Drawing.Font("Britannic Bold", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(118, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(171, 27);
            this.label2.TabIndex = 3;
            this.label2.Text = "y Computación";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.label1.Font = new System.Drawing.Font("Britannic Bold", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(116, 61);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(260, 38);
            this.label1.TabIndex = 2;
            this.label1.Text = "Hermano Pedro";
            // 
            // text1
            // 
            this.text1.AutoSize = true;
            this.text1.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.text1.Font = new System.Drawing.Font("Britannic Bold", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text1.ForeColor = System.Drawing.Color.White;
            this.text1.Location = new System.Drawing.Point(116, 6);
            this.text1.Name = "text1";
            this.text1.Size = new System.Drawing.Size(300, 27);
            this.text1.TabIndex = 1;
            this.text1.Text = "Academia de Mecanografia";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Main.FrontEnd.Properties.Resources.logo_hp;
            this.pictureBox1.Location = new System.Drawing.Point(12, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(97, 94);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // panelcontenido
            // 
            this.panelcontenido.Controls.Add(this.contenido);
            this.panelcontenido.Location = new System.Drawing.Point(0, 449);
            this.panelcontenido.Name = "panelcontenido";
            this.panelcontenido.Size = new System.Drawing.Size(1350, 250);
            this.panelcontenido.TabIndex = 6;
            this.panelcontenido.Resize += new System.EventHandler(this.rcentrarc);
            // 
            // contenido
            // 
            this.contenido.Controls.Add(this.panel4);
            this.contenido.Controls.Add(this.panel3);
            this.contenido.Controls.Add(this.panel2);
            this.contenido.Location = new System.Drawing.Point(300, -1);
            this.contenido.Name = "contenido";
            this.contenido.Size = new System.Drawing.Size(750, 200);
            this.contenido.TabIndex = 0;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(75)))), ((int)(((byte)(72)))));
            this.panel4.Controls.Add(this.pictureBox4);
            this.panel4.Controls.Add(this.InfoAlumno);
            this.panel4.Location = new System.Drawing.Point(500, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(250, 200);
            this.panel4.TabIndex = 2;
            this.panel4.MouseHover += new System.EventHandler(this.panel4_MouseHover);
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::Main.FrontEnd.Properties.Resources.logoestudiante;
            this.pictureBox4.Location = new System.Drawing.Point(15, 5);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(73, 73);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox4.TabIndex = 6;
            this.pictureBox4.TabStop = false;
            this.pictureBox4.MouseHover += new System.EventHandler(this.panel4_MouseHover);
            // 
            // InfoAlumno
            // 
            this.InfoAlumno.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(75)))), ((int)(((byte)(72)))));
            this.InfoAlumno.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.InfoAlumno.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.InfoAlumno.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.InfoAlumno.Location = new System.Drawing.Point(94, 0);
            this.InfoAlumno.Name = "InfoAlumno";
            this.InfoAlumno.ReadOnly = true;
            this.InfoAlumno.Size = new System.Drawing.Size(156, 200);
            this.InfoAlumno.TabIndex = 7;
            this.InfoAlumno.Text = "Registro de los alumnos inscritos, reportes, visualizacion de notas, estadisticas" +
    " generales.";
            this.InfoAlumno.MouseHover += new System.EventHandler(this.panel4_MouseHover);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(63)))), ((int)(((byte)(175)))));
            this.panel3.Controls.Add(this.pictureBox2);
            this.panel3.Controls.Add(this.InfoCurso);
            this.panel3.Location = new System.Drawing.Point(250, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(250, 200);
            this.panel3.TabIndex = 1;
            this.panel3.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel3_MouseMove);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::Main.FrontEnd.Properties.Resources.logomateria;
            this.pictureBox2.Location = new System.Drawing.Point(6, 3);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(76, 75);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 4;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel3_MouseMove);
            // 
            // InfoCurso
            // 
            this.InfoCurso.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(63)))), ((int)(((byte)(175)))));
            this.InfoCurso.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.InfoCurso.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.InfoCurso.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.InfoCurso.Location = new System.Drawing.Point(88, 0);
            this.InfoCurso.Name = "InfoCurso";
            this.InfoCurso.ReadOnly = true;
            this.InfoCurso.Size = new System.Drawing.Size(159, 197);
            this.InfoCurso.TabIndex = 5;
            this.InfoCurso.Text = "Lleve un registro con el detalle de los cursos, estadisticas y horarios ";
            this.InfoCurso.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel3_MouseMove);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(181)))), ((int)(((byte)(181)))));
            this.panel2.Controls.Add(this.pictureBox3);
            this.panel2.Controls.Add(this.InfoCatedratico);
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(253, 200);
            this.panel2.TabIndex = 0;
            this.panel2.MouseHover += new System.EventHandler(this.panel2_MouseHover);
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::Main.FrontEnd.Properties.Resources.logocatedratico;
            this.pictureBox3.Location = new System.Drawing.Point(15, 5);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(73, 73);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 6;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.MouseHover += new System.EventHandler(this.panel2_MouseHover);
            // 
            // InfoCatedratico
            // 
            this.InfoCatedratico.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(181)))), ((int)(((byte)(181)))));
            this.InfoCatedratico.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.InfoCatedratico.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.InfoCatedratico.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.InfoCatedratico.Location = new System.Drawing.Point(95, 3);
            this.InfoCatedratico.Name = "InfoCatedratico";
            this.InfoCatedratico.ReadOnly = true;
            this.InfoCatedratico.Size = new System.Drawing.Size(155, 197);
            this.InfoCatedratico.TabIndex = 3;
            this.InfoCatedratico.Text = "Lleve un registro del personal docente. Con datos, registros, informes y detalle " +
    "de las asignaciones";
            this.InfoCatedratico.MouseHover += new System.EventHandler(this.panel2_MouseHover);
            // 
            // imgback
            // 
            this.imgback.Image = global::Main.FrontEnd.Properties.Resources.bg;
            this.imgback.Location = new System.Drawing.Point(0, 100);
            this.imgback.Name = "imgback";
            this.imgback.Size = new System.Drawing.Size(1350, 300);
            this.imgback.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgback.TabIndex = 5;
            this.imgback.TabStop = false;
            this.imgback.Click += new System.EventHandler(this.imgback_Click);
            // 
            // MenudelAdministrador
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1350, 729);
            this.Controls.Add(this.panelcontenido);
            this.Controls.Add(this.imgback);
            this.Controls.Add(this.namearea);
            this.Controls.Add(this.botonarea);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(750, 39);
            this.Name = "MenudelAdministrador";
            this.Text = "Academia de Mecanografia y Computacion Hermano Pedro ";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Resize += new System.EventHandler(this.sizedistribution);
            this.botonarea.ResumeLayout(false);
            this.botons.ResumeLayout(false);
            this.namearea.ResumeLayout(false);
            this.namearea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panelcontenido.ResumeLayout(false);
            this.contenido.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgback)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnRegistroDeAlumnos;
        private System.Windows.Forms.Button btnRegistroDeCatedrativos;
        private System.Windows.Forms.Panel botonarea;
        private System.Windows.Forms.Panel namearea;
        private System.Windows.Forms.Panel botons;
        private System.Windows.Forms.PictureBox imgback;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label text1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panelcontenido;
        private System.Windows.Forms.Panel contenido;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.RichTextBox InfoCatedratico;
        private System.Windows.Forms.RichTextBox InfoAlumno;
        private System.Windows.Forms.RichTextBox InfoCurso;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
    }
}


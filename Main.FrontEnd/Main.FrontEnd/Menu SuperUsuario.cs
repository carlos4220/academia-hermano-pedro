﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CreacionAsignación;

namespace Main.FrontEnd
{
    public partial class Menu_SuperUsuario : Form
    {
        public Menu_SuperUsuario()
        {
            InitializeComponent();
        }

        private void BtnUsusarios_Click(object sender, EventArgs e)
        {
            //SE LLAMA EL FORMULARIO DE LA GESTION DE USUARIOS
            Asignacion_Curso ventana = new Asignacion_Curso();
            ventana.ShowDialog();
        }

        private void BtnReportes_Click(object sender, EventArgs e)
        {
            //SE LLAMA EL FORMULARIO DE REPORTES
        }
    }
}

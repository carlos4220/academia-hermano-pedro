﻿namespace Main.FrontEnd
{
    partial class Menu_SuperUsuario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnUsusarios = new System.Windows.Forms.Button();
            this.btnReportes = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnUsusarios
            // 
            this.btnUsusarios.Location = new System.Drawing.Point(137, 86);
            this.btnUsusarios.Name = "btnUsusarios";
            this.btnUsusarios.Size = new System.Drawing.Size(111, 52);
            this.btnUsusarios.TabIndex = 0;
            this.btnUsusarios.Text = "Gestion de usuarios";
            this.btnUsusarios.UseVisualStyleBackColor = true;
            this.btnUsusarios.Click += new System.EventHandler(this.BtnUsusarios_Click);
            // 
            // btnReportes
            // 
            this.btnReportes.Location = new System.Drawing.Point(137, 166);
            this.btnReportes.Name = "btnReportes";
            this.btnReportes.Size = new System.Drawing.Size(111, 52);
            this.btnReportes.TabIndex = 1;
            this.btnReportes.Text = "Generar Reportes";
            this.btnReportes.UseVisualStyleBackColor = true;
            this.btnReportes.Click += new System.EventHandler(this.BtnReportes_Click);
            // 
            // Menu_SuperUsuario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(394, 360);
            this.Controls.Add(this.btnReportes);
            this.Controls.Add(this.btnUsusarios);
            this.Name = "Menu_SuperUsuario";
            this.Text = "Menu_SuperUsuario";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnUsusarios;
        private System.Windows.Forms.Button btnReportes;
    }
}
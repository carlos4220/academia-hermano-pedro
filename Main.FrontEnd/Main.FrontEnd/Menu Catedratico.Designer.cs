﻿namespace Main.FrontEnd
{
    partial class Menu_Catedratico
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Menu_Catedratico));
            this.btnCalificaciones = new System.Windows.Forms.Button();
            this.btnListadoDeAlumnos = new System.Windows.Forms.Button();
            this.btnPanelDePublicaciones = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.SuspendLayout();
            // 
            // btnCalificaciones
            // 
            this.btnCalificaciones.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            this.btnCalificaciones.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCalificaciones.Font = new System.Drawing.Font("Verdana", 13.25F, System.Drawing.FontStyle.Bold);
            this.btnCalificaciones.ForeColor = System.Drawing.Color.White;
            this.btnCalificaciones.Location = new System.Drawing.Point(159, 178);
            this.btnCalificaciones.Name = "btnCalificaciones";
            this.btnCalificaciones.Size = new System.Drawing.Size(258, 65);
            this.btnCalificaciones.TabIndex = 0;
            this.btnCalificaciones.Text = "Mostrar Calificaciones";
            this.btnCalificaciones.UseVisualStyleBackColor = false;
            this.btnCalificaciones.Click += new System.EventHandler(this.BtnCalificaciones_Click);
            // 
            // btnListadoDeAlumnos
            // 
            this.btnListadoDeAlumnos.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(187)))), ((int)(((byte)(187)))));
            this.btnListadoDeAlumnos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnListadoDeAlumnos.Font = new System.Drawing.Font("Verdana", 13.25F, System.Drawing.FontStyle.Bold);
            this.btnListadoDeAlumnos.ForeColor = System.Drawing.Color.White;
            this.btnListadoDeAlumnos.Location = new System.Drawing.Point(159, 249);
            this.btnListadoDeAlumnos.Name = "btnListadoDeAlumnos";
            this.btnListadoDeAlumnos.Size = new System.Drawing.Size(258, 65);
            this.btnListadoDeAlumnos.TabIndex = 1;
            this.btnListadoDeAlumnos.Text = "Listado de Alumnos";
            this.btnListadoDeAlumnos.UseVisualStyleBackColor = false;
            this.btnListadoDeAlumnos.Click += new System.EventHandler(this.BtnListadoDeAlumnos_Click);
            // 
            // btnPanelDePublicaciones
            // 
            this.btnPanelDePublicaciones.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(217)))), ((int)(((byte)(217)))));
            this.btnPanelDePublicaciones.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPanelDePublicaciones.Font = new System.Drawing.Font("Verdana", 13.25F, System.Drawing.FontStyle.Bold);
            this.btnPanelDePublicaciones.ForeColor = System.Drawing.Color.White;
            this.btnPanelDePublicaciones.Location = new System.Drawing.Point(159, 320);
            this.btnPanelDePublicaciones.Name = "btnPanelDePublicaciones";
            this.btnPanelDePublicaciones.Size = new System.Drawing.Size(258, 65);
            this.btnPanelDePublicaciones.TabIndex = 2;
            this.btnPanelDePublicaciones.Text = "Panel de Publicaciones";
            this.btnPanelDePublicaciones.UseVisualStyleBackColor = false;
            this.btnPanelDePublicaciones.Click += new System.EventHandler(this.BtnPanelDePublicaciones_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(181)))), ((int)(((byte)(181)))));
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.pictureBox4);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(585, 160);
            this.panel1.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.label2.Font = new System.Drawing.Font("Britannic Bold", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(164, 98);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(231, 38);
            this.label2.TabIndex = 9;
            this.label2.Text = "CATEDRATICO";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.label1.Font = new System.Drawing.Font("Britannic Bold", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(164, 46);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(104, 38);
            this.label1.TabIndex = 8;
            this.label1.Text = "MENU";
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::Main.FrontEnd.Properties.Resources.logocatedratico;
            this.pictureBox4.Location = new System.Drawing.Point(12, 12);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(146, 136);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox4.TabIndex = 7;
            this.pictureBox4.TabStop = false;
            // 
            // Menu_Catedratico
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(145)))), ((int)(((byte)(144)))));
            this.ClientSize = new System.Drawing.Size(584, 411);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnPanelDePublicaciones);
            this.Controls.Add(this.btnListadoDeAlumnos);
            this.Controls.Add(this.btnCalificaciones);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(600, 450);
            this.Name = "Menu_Catedratico";
            this.Text = "Menu_Catedratico";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnCalificaciones;
        private System.Windows.Forms.Button btnListadoDeAlumnos;
        private System.Windows.Forms.Button btnPanelDePublicaciones;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox4;
    }
}
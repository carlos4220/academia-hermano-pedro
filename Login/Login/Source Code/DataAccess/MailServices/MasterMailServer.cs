﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;
using System.Net;

namespace DataAccess.MailServices
{
   public abstract class MasterMailServer
    {
        //Atributo principal./ Main attribute
        private SmtpClient smtpClient;
        //Atributos del cliente SMTP.//Client SMTP Attributes
        protected string senderMail { get; set; }
        protected string password { get; set; }
        protected string host { get; set; }
        protected int port { get; set; }
        protected bool ssl { get; set; }

        //Inicializamos las propiedades del cliente SMTP, con los atributos anteriores.
       //Initialize the properties of the SMTP client, with the previous attributes.
        protected void initializeSmtpClient() {
            smtpClient = new SmtpClient();
            smtpClient.Credentials = new NetworkCredential(senderMail,password);
            smtpClient.Host = host;
            smtpClient.Port = port;
            smtpClient.EnableSsl = ssl;
        }

//Metodo para enviar mensajes de correo a uno o varios destinatarios./El parametro correo receptor es de tipo lista, porque en muchas ocaciones es necesario enviar correos por lote(varios a la vez), pero es opcional, puedes quitar lista, y enviar solo a un correo destinatario.
//Method to send the email messages to one or more recipients.       /The receiverMail parameter Is a list type, because in many occasions it Is necessary to send mails by lot (several at the same time), but it Is optional, you can remove list, And send only to a recipient mail.
        public void sendMail(string subject,string body, List<string> recipientMail) {
            //create mail message// creamos el mensaje de correo.
            var mailMessage = new MailMessage();
            try
            {
                mailMessage.From = new MailAddress(senderMail);
                foreach (string mail in recipientMail)
                {
                    mailMessage.To.Add(mail);
                }
                mailMessage.Subject = subject;
                mailMessage.Body = body;
                mailMessage.Priority = MailPriority.Normal;
                smtpClient.Send(mailMessage); //send message//Enviar Mensaje
            }
            catch (Exception ex) { }
            finally {
                mailMessage.Dispose();
                smtpClient.Dispose();
            }
        }
    }
}

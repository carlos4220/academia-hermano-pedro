﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using Domain;
using Common.Cache;
using Main;

namespace Presentation
{
    public partial class FormLogin : Form
    {
        public FormLogin()
        {
            InitializeComponent();
        }
        #region Form Methods
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hwnd, int wmsg, int wparam, int lparam);


        private void txtuser_Enter(object sender, EventArgs e)
        {
            if (txtuser.Text == "Nombre de Usuario o Correo")
            {
                txtuser.Text = "";
                txtuser.ForeColor = Color.WhiteSmoke;
            }
        }

        private void txtuser_Leave(object sender, EventArgs e)
        {
            if (txtuser.Text == "")
            {
                txtuser.Text = "Nombre de Usuario o Correo";
                txtuser.ForeColor = Color.Silver;
            }
        }

        private void txtpass_Enter(object sender, EventArgs e)
        {
            if (txtpass.Text == "Contraseña")
            {
                txtpass.Text = "";
                txtpass.ForeColor = Color.WhiteSmoke;
                txtpass.UseSystemPasswordChar = true;
            }
        }

        private void txtpass_Leave(object sender, EventArgs e)
        {
            if (txtpass.Text == "")
            {
                txtpass.Text = "Contraseña";
                txtpass.ForeColor = Color.Silver;
                txtpass.UseSystemPasswordChar = false;
            }
        }

        private void btncerrar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnminimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }
        #endregion

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void btnlogin_Click(object sender, EventArgs e)
        {

            //<esto se ingreso con fines demostrativos para poder mostrar el main
            if(txtuser.Text == "adm" && txtpass.Text == "adm")
            {
                this.Hide();
                Main.FrontEnd.MenudelAdministrador main = new Main.FrontEnd.MenudelAdministrador();
                FormWelcome welcome = new FormWelcome();
                welcome.ShowDialog();
                main.Show();
                main.FormClosed += Logout;
            }
            else if(txtuser.Text == "alumno" && txtpass.Text == "alumno")
            {
                this.Hide();
                Main.FrontEnd.Menu_Alumno main = new Main.FrontEnd.Menu_Alumno();
                FormWelcome welcome = new FormWelcome();
                welcome.ShowDialog();
                main.Show();
                main.FormClosed += Logout;
            }
            else if (txtuser.Text == "profesor" && txtpass.Text == "profesor")
            {
                this.Hide();
                Main.FrontEnd.Menu_Catedratico main = new Main.FrontEnd.Menu_Catedratico();
                FormWelcome welcome = new FormWelcome();
                welcome.ShowDialog();
                main.Show();
                main.FormClosed += Logout;
            }
            else if (txtuser.Text == "super" && txtpass.Text == "super")
            {
                this.Hide();
                Main.FrontEnd.Menu_SuperUsuario main = new Main.FrontEnd.Menu_SuperUsuario();
                FormWelcome welcome = new FormWelcome();
                welcome.ShowDialog();
                main.Show();
                main.FormClosed += Logout;
            }
            else
            {
                msgError("Contraseña o Usuario Incorrecto. \n   Intente de Nuevo.");
                txtpass.Text = "Contraseña";
                txtpass.UseSystemPasswordChar = false;
                txtuser.Focus();
            }
            //<esto se ingreso con fines demostrativos para poder mostrar el main>

            //este bloque de codigo sera adaptado solo cuando se tenga la conexion a la base de datos hosteada
            /*
            if (txtuser.Text != "Nombre de Usuario o Correo" && txtuser.TextLength > 2)
            {
                if (txtpass.Text != "Contraseña")
                {
                    UserModel user = new UserModel();
                    var validLogin = user.Login(txtuser.Text, txtpass.Text);
                    if (validLogin == true)
                    {
                        this.Hide();
                        FormMainMenu mainMenu = new FormMainMenu();
                        FormWelcome welcome = new FormWelcome();
                        welcome.ShowDialog();                        
                        mainMenu.Show();
                        mainMenu.FormClosed += Logout;

                    }
                    else {
                        msgError("Contraseña o Usuario Incorrecto. \n   Intente de Nuevo.");
                        txtpass.Text = "Contraseña";
                        txtpass.UseSystemPasswordChar = false;
                        txtuser.Focus();
                    }
                }
                else msgError("Por favor ingrese su Contraseña.");
            }
            else msgError("Por favor ingrese su Nombre de Usuario o Correo.");
            */
        }
        private void msgError(string msg)
        {
            lblErrorMessage.Text = "    " + msg;
            lblErrorMessage.Visible = true;
        }
        private void Logout(object sender, FormClosedEventArgs e)
        {
            txtpass.Text = "Contraseña";
            txtpass.UseSystemPasswordChar = false;
            txtuser.Text = "Nombre de Usuario o Correo";
            lblErrorMessage.Visible = false;
            this.Show();
        }

        private void linkpass_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            var recoverPassword = new FormRecoverPassword();
            recoverPassword.ShowDialog();
        }

        private void linkSignUp_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            var formRegisterUser = new FormCreateUser();
            formRegisterUser.ShowDialog();
        }

        private void txtuser_TextChanged(object sender, EventArgs e)
        {

        }

        private void lblErrorMessage_Click(object sender, EventArgs e)
        {

        }

        private void txtpass_TextChanged(object sender, EventArgs e)
        {

        }
    }
}

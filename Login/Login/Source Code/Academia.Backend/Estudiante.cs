﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Academia.Backend
{
    public class Estudiante:Persona
    {
        public List<AsignacionCursoAlumno> CursosAsignador { get; set; }
        public Estudiante()
        {
            CursosAsignador = new List<AsignacionCursoAlumno>();
        }
        public string IngresarDatos()
        {
            return " ";
        }
    }
}

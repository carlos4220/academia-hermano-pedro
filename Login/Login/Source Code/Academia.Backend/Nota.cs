﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Academia.Backend
{
    public class Nota
    {
        public int NotaActitudinal { get; set; }
        public int NotaTarea { get; set; }
        public int NotaExamen { get; set; }
        public int NotaEjecicio { get; set; }
    }
}
